#include <ezButton.h>

class RGBLed {
private:
  uint8_t r = 0;
  uint8_t g = 0;
  uint8_t b = 0;

public:

  void red(uint8_t brightness) {
    r = brightness * 2.55;
    g = 0;
    b = 0;
    neopixelWrite(RGB_BUILTIN, r, g, b);
  }
  void green(uint8_t brightness) {
    r = 0;
    g = brightness * 2.55;
    b = 0;
    neopixelWrite(RGB_BUILTIN, r, g, b);
  }
  void blue(uint8_t brightness) {
    r = 0;
    g = 0;
    b = brightness * 2.55;
    neopixelWrite(RGB_BUILTIN, r, g, b);
  }
  void white(uint8_t brightness) {
    r = brightness * 2.55;
    g = brightness * 2.55;
    b = brightness * 2.55;
    neopixelWrite(RGB_BUILTIN, r, g, b);
  }
  void off() {
    r = 0;
    g = 0;
    b = 0;
    digitalWrite(RGB_BUILTIN, LOW);
  }
  void custom(uint8_t R, uint8_t G, uint8_t B) {
    r = R;
    g = G;
    b = B;
    neopixelWrite(RGB_BUILTIN, r, g, b);
  }
  void custom_transition(uint8_t R, uint8_t G, uint8_t B) {
    while (r != R || g != G || b != B) {
      if (r < R) r += 1;
      if (r > R) r -= 1;

      if (g < G) g += 1;
      if (g > G) g -= 1;

      if (b < B) b += 1;
      if (b > B) b -= 1;

      neopixelWrite(RGB_BUILTIN, r, g, b);
      delay(50);
    }
  }
  void rainbow(uint8_t brightness) {
    custom_transition(brightness * 2.55, 0, 0);                  // red
    custom_transition(brightness * 2.55, brightness * 2.55, 0);  // yellow
    custom_transition(0, brightness * 2.55, 0);                  // green
    custom_transition(0, brightness * 2.55, brightness * 2.55);  // cyan
    custom_transition(0, 0, brightness * 2.55);                  // blue
    custom_transition(brightness * 2.55, 0, brightness * 2.55);  // magenta
    custom_transition(brightness * 2.55, 0, 0);                  // red
    custom_transition(0, 0, 0);                                  // dark
  }
};


RGBLed rgb;
ezButton bootton(0);
ezButton button14(14);

void setup() {
  Serial.begin(115200);
  bootton.setDebounceTime(50);
  button14.setDebounceTime(50);
}

void loop() {
  bootton.loop();
  button14.loop();

  if (button14.isReleased()) {
    Serial.println("pressed_button14");
    rgb.rainbow(6);
  } else if (bootton.isReleased()) {
    Serial.println("pressed_bootton");
    for (int i = 1; i < 101; i++) {
      rgb.white(i);
      Serial.printf("Brightness: %d\n", i);
      delay(1000 / i);
    }
    rgb.off();
  }
  
}